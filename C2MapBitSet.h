/* C2MapBitSet - класс предназначен для работы с парами, каждая из которых
состоит из двух элементов - множества чисел и числа. При этом допустимо искать
пару, как по первому элементу, так и по второму. Пары хранятся в виде АВЛ -
дерева */
class C2MapBitSet {
    private:
        map<CBitSet, int> m_first_sort;
        // Дерево, отсортированное по первому ключу
        map<int, CBitSet> m_second_sort;
        // Дерево, отсортированное по второму ключу
    public:
        C2MapBitSet(void);
        ~C2MapBitSet(void);
        void add(const CBitSet& fs, const int sec);
        // Добавить пару
        int findByFirst(const CBitSet& fs);
        // Найти по первому ключу
        CBitSet const* findBySecond(const int sec);
        // Найти по второму ключу
        void clear();
        // Очистить от всех ключей
}; 

typedef map<CBitSet, int> MAPTOFS;
typedef map<int, CBitSet> MAPTOSEC;

C2MapBitSet::C2MapBitSet(void) {}
C2MapBitSet::~C2MapBitSet(void) {}

// Очистить от всех ключей
void C2MapBitSet::clear(){
    m_first_sort.clear();
    m_second_sort.clear();
}

// Найти по первому ключу
int C2MapBitSet::findByFirst(const CBitSet& fs) {
    MAPTOFS::iterator it = m_first_sort.find(fs);
    if (it == m_first_sort.end()) return -1;
    return it->second;
}

// Найти по второму ключу
CBitSet const* C2MapBitSet::findBySecond(const int sec){
    MAPTOSEC::iterator it = m_second_sort.find(sec);
    if (it == m_second_sort.end()) return NULL;
    return &(it->second);
}

// Добавить пару
void C2MapBitSet::add(const CBitSet& fs, const int sec) {
    m_first_sort.insert(MAPTOFS::value_type(fs, sec));
    m_second_sort.insert(MAPTOSEC::value_type(sec, fs));
} 