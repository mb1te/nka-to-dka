/* CAutomaton - класс, который хранит автомат. Он позволяет считывать из потока
данных описание НКА, проверять детерминированность, приводить к ней и удалять
недостижимые (лишние) состояния, а также выводить полученное описание ДКА в
поток */

class CAutomaton
{
    friend class CState;
    private:
        void scanState(CLexems *lex);
        // Считывание состояния из потока lex

        void scanAutomaton(CLexems *lex);
        // Считывание автомата из потока lex

        void scanLink(CLexems *lex, CState *state);
        // Считывание переходов из потока lex и запись их в состояние state

        char* m_automateName; // Имя автомата
        CListWord* m_statesnames; // Набор имен состояний
        CListWord* m_eventsnames; // Набор имен событий
        vector<CState*> m_vstates;
        // Вектор содержит состояния, в которых хранятся
        // только номера, и никаких имен

        void createState();
        // Создать состояние
    public:
        CAutomaton(void);
        CAutomaton(const char* name);
        ~CAutomaton(void);
        int countStates(); // Количество состояний
        int countEvents(); // Количество входных воздействий
        int countUnachievable(); // Количество недостижимых состояний

        void getNamesStates(vector<const char*>& list);
        // Получить список имен состояний

        void getNamesEvents(vector<const char*>& list);
        // Получить список имен событий

        void getNamesUnachievable(vector<const char*>& list);
        // Получить список имен недостижимых вершин

        CState* getStateByIndex(int index);
        // Получить состояние по его id (индексу)

        CState* getStateByName(const char* name);
        // Получить состояние по его имени

        const char* getStateNameByIndex(int index);
        // Получить имя состояния по его индексу

        int getEventIndexByName(const char* name);
        // Получить индекс события по имени

        const char* getEventNameByIndex(int idevent);
        // Получить имя входного воздействия по индексу

        CBitSet* findAchievable();
        // Найти множество всех достижимых вершин

        bool loadFromFile(const char *name);
        // Считать описание автомата из файла

        bool saveToFile(const char *name);
        // Сохранить описание автомата в файл

        bool checkDeterminate();
        // Проверить детерминированность

        bool turnToDeterminate();
        // Привести к детерминированному

        void clearAutomaton();
        // “Очистить” автомат

        CState* createStateRandom();
        // Создать состояние со "случайным" именем

        bool deleteUnachievable();
        // Удалить недостижимые вершины

        void deleteStatesByMask(CBitSet* set);
        // Удалить состояния по маске

        ostream& operator<< (ostream&);
        // Вывести описание автомата в поток
        
        istream& operator>> (istream&);
        // Считать описание автомата из потока
};

ostream& operator<< (ostream& out, CAutomaton* state);
ostream& operator<< (ostream& out, CAutomaton& state);
istream& operator>> (istream& sin, CAutomaton* state);
istream& operator>> (istream& sin, CAutomaton& state); 

char* NAMEELSEEVENT = "ELSE";
char* NAMEANYEVENT = "ANY";

CAutomaton::CAutomaton(void) {
    m_eventsnames = new CListWord();
    m_statesnames = new CListWord();
    m_vstates.clear();
    m_automateName = NULL;
    int IDANYEVENT = getEventIndexByName(NAMEANYEVENT);
    int IDELSEEVENT = getEventIndexByName(NAMEELSEEVENT);
}

CAutomaton::CAutomaton(const char* name) {
    m_eventsnames = new CListWord();
    m_statesnames = new CListWord();
    m_vstates.clear();
    m_automateName = strdup(name);
    int IDANYEVENT = getEventIndexByName(NAMEANYEVENT);
    int IDELSEEVENT = getEventIndexByName(NAMEELSEEVENT);
}

CAutomaton::~CAutomaton(void) {
    clearAutomaton();
    delete m_eventsnames;
    delete m_statesnames;
} 

// Количество состояний
int CAutomaton::countStates() {
    return m_vstates.size();
}

// Количество событий
int CAutomaton::countEvents() {
    return m_eventsnames->getCount();
}

// Количество недостижимых состояний
int CAutomaton::countUnachievable() {
    CBitSet* reach = findAchievable();
    int cnt = 0;
    for (int i = 0; i < countStates(); i++)
    if (!reach->check(i)) cnt++;
    delete reach;
    return cnt;
}

// Получить состояние по его id (индексу)
CState* CAutomaton::getStateByIndex(int index) {
    if ((index >= 0) && (index < countStates())) return m_vstates[index];
    else return NULL;
}

// Создать состояние
void CAutomaton::createState() {
    m_vstates.push_back(new CState(this, countStates()));
} 

// Получить состояние по его имени
CState* CAutomaton::getStateByName(const char* name) {
    int index = m_statesnames->addWord(name); // !?
    if (index >= countStates()) createState();
    return getStateByIndex(index);
}

// Получить индекс события по имени
int CAutomaton::getEventIndexByName(const char* name) {
    return m_eventsnames->addWord(name);
}

// Получить имя события по индексу
const char* CAutomaton::getEventNameByIndex(int idevent) {
    return m_eventsnames->findById(idevent);
}

// Получить имя состояния по его индексу
const char* CAutomaton::getStateNameByIndex(int index) {
    return m_statesnames->findById(index);
}

// Создать состояние со "случайным" именем
CState* CAutomaton::createStateRandom() {
    CString ss;
    ss.Format("CState%i", countStates());
    return getStateByName(ss);
}

// Считывание переходов из потока lex и запись их в состояние state
void CAutomaton::scanLink(CLexems *lex, CState *state) {
    lex->compare(lex -> getCurrentLexem(), TL_IDENTY);
    char *eventname = strdup(lex->getString());
    lex->compare(lex -> nextLexem(), TL_IDENTY); 
    CState* idstate = getStateByName(lex->getString());
    state->addCrossing(getEventIndexByName(eventname), idstate->getThisId());
    lex->compare(lex -> nextLexem(), TL_SEMICOLON);
    lex -> nextLexem();
    delete eventname;
}

// Считывание состояния из потока lex
void CAutomaton::scanState(CLexems *lex) {
    bool w_start = false, w_final = false, w_state = false, w_def = false;
    while (!w_def) {
        switch (lex -> getCurrentLexem()) {
            case TL_KEYSTART:
            if (w_start) 
                lex->throwBad("Double start definition");
                w_start = true; 
                break;
            case TL_KEYFINAL:
                if (w_final) lex->throwBad("Double final definition");
                w_final = true; 
                break;
            case TL_KEYSTATE:
                if (w_state) lex->throwBad("Double state definition");
                w_state = true; 
                break;
            case TL_IDENTY: 
                w_def = true; 
                break;
            default: 
                lex->throwBad("Strange symbol defined");
                break;
        };
        lex->nextLexem();
    };
    if (!w_state) lex->throwBad("State not defined");
    CState *state;
    state = getStateByName(lex->getString());
    state->setFinalProperty(w_final);
    state->setStartProperty(w_start);
    lex->compare(lex -> getCurrentLexem(), TL_OPENBRACE);
    lex->nextLexem();
    while (lex -> getCurrentLexem() != TL_CLOSEBRACE) scanLink(lex, state); 
    state->processAnyState();
    extern IDELSEEVENT;
    if (!state->checkELSEEVENTLink()) state->addCrossing(IDELSEEVENT, state->getThisId());
    lex -> nextLexem();
}

// Считывание автомата из потока lex
void CAutomaton::scanAutomaton(CLexems *lex) {
    clearAutomaton();
    lex->compare(lex->getCurrentLexem(), TL_IDENTY);
    m_automateName = strdup(lex->getString());
    lex->compare(lex->nextLexem(), TL_OPENBRACE);
    lex->nextLexem();
    while (lex->getCurrentLexem() != TL_CLOSEBRACE) scanState(lex) ;
    lex->compare(lex->nextLexem(), TL_ENDFILE);
}

// Очистить автомат
void CAutomaton::clearAutomaton() {
    for (int i = 0 ; i < countStates(); i++) delete m_vstates[i];
    m_vstates.clear();
    int IDANYEVENT = getEventIndexByName(NAMEANYEVENT);
    int IDELSEEVENT = getEventIndexByName(NAMEELSEEVENT);
    delete m_automateName;
    m_automateName = NULL;
}

// Считать описание автомата из файла
bool CAutomaton::loadFromFile(const char* name) { 
    try {
        ifstream fin(name);
        fin >> (*this);
        fin.close();
    }
    catch (...) {
        clearAutomaton();
        return false;
    };
    return true;
}

// Сохранить описание автомата в файл
bool CAutomaton::saveToFile(const char *name) {
    try {
        ofstream output(name);
        if (!output) throw "File is not accessible";
        output << (*this);
    output.close();
    }
    catch (...) { 
        return false; 
    };
    return true;
}

// Проверить детерминированность
bool CAutomaton::checkDeterminate() {
    for (int i = 0; i < countStates(); i++)
    if (!(getStateByIndex(i)->checkStateDeterminate())) return false;
    return true;
}

// Привести к детерминированному
bool CAutomaton::turnToDeterminate() {
    C2MapBitSet listenum;
    CBitSet a = CBitSet(countStates());
    a.reset();
    for (int i = 0; i < countStates(); i++) {
        a.set(i);
        listenum.add(a, i);
        a.reset(i);
    };
    int cnt = countStates();
    i = 0;
    while (i < countStates()) {
        CState* state;
        state = getStateByIndex(i);
        if (!(state->checkStateDeterminate())) {
            if (!state->transformToDeterminate(&listenum, cnt)) return false;
        };
        i++;
    }
    return true;
}

// Найти множество всех достижимых вершин
CBitSet* CAutomaton::findAchievable() {
    bool existanystart = false;
    CBitSet* reach = new CBitSet(countStates());
    reach->reset();
    for (int i = 0; i < countStates(); i++) {
        CState* state = getStateByIndex(i);
        if (state->checkStartProperty()) {
            state->goneRound(reach);
            existanystart = true;
        };
    }
    if (!existanystart) {
        for (int i = 0; i < countStates(); i++) reach->set(i);
    };
    return reach;
}

// Удалить состояния по маске
void CAutomaton::deleteStatesByMask(CBitSet* set) {
    if (set->getLength() != countStates()) return; 
    vector<int> newids(countStates());
    int newid = 0;
    for (int i = 0; i < countStates(); i++) {
        if (set->check(i)) {
            newids[i] = newid;
            newid ++;
        } 
        else newids[i] = -1;
        m_statesnames->deleteByMask(set);
        deleteVectorByMask<CState> (&m_vstates, set);
        for (i = 0; i<countStates(); i++) {
            m_vstates[i]->m_myOwnId = i;
            m_vstates[i]->refreshLinks(newids);
        };
    }
}

// Удалить недостижимые вершины
bool CAutomaton::deleteUnachievable() {
    CBitSet* reach = findAchievable();
    deleteStatesByMask(reach);
    delete reach;
    return true;
}

// Вывести описание автомата в поток
ostream& operator<< (ostream& out, CAutomaton* aut) {
    return (aut->operator <<(out));
}

// Вывести описание автомата в поток
ostream& operator<< (ostream& out, CAutomaton& aut) {
    return (aut.operator <<(out));
}

// Вывести описание автомата в поток
ostream& CAutomaton::operator<< (ostream& out) {
    out << m_automateName << " {\n\n"; 
    for (int i = 0; i < countStates(); i++) {
        CState *state = getStateByIndex(i);
        out << state << "\n";
    };
    out << "} // End of " << m_automateName;
    return out;
}

// Считать описание автомата из потока
istream& operator>> (istream& sin, CAutomaton* aut) {
    return (aut->operator >>(sin));
}

// Считать описание автомата из потока
istream& operator>> (istream& sin, CAutomaton& aut) {
    return (aut.operator >>(sin));
}

// Считать описание автомата из потока
istream& CAutomaton::operator>> (istream& sin) {
    CLexems* lex;
    try {
        lex = new CLexems(sin);
        scanAutomaton(lex);
        delete lex;
    }
    catch (CString err) {
        if (lex != NULL) delete lex;
        clearAutomaton();
        throw err;
    };
    return sin;
}

// Получить список имен состояний
void CAutomaton::getNamesStates(vector<const char*>& list) {
    m_statesnames->getVectorNames(list);
} 

// Получить список имен событий
void CAutomaton::getNamesEvents(vector<const char*>& list) {
    m_eventsnames->getVectorNames(list);
}

// Получить список имен недостижимых вершин
void CAutomaton::getNamesUnachievable(vector<const char*>& list) {
    CBitSet* reach = findAchievable();
    list.clear();
    for (int i = 0; i < countStates(); i++)
    if (!reach->check(i)) list.push_back(m_statesnames->findById(i));
    delete reach;
} 