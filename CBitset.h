/* CBitSet - класс предназначен для работы с множествами (объединение, включение
и исключение элементов) */

class CBitSet {
private:
    int m_length; // Элемент уже не допускаемый множеством
    bool* m_set; // Булевый вектор - представление множества
public:
    CBitSet(int nlength);
    // Создать множество, допускающее элементы от 0..nlength-1

    CBitSet(const CBitSet& right);
    // Конструктор присваивания

    CBitSet& operator= (const CBitSet& right);
    // Конструктор копирования

    int getLength() const;
    // Узнать размерность множества

    bool check(int index) const;
    // Проверить наличие в множестве элемента index

    void set(int index);
    // Включить в множество элемент index

    void reset(int index);
    // Исключить элемент index из множества

    void reset();
    // Исключить все элементы

    bool operator< (const CBitSet &right) const;
    // Сравнить два множества

    operator += (const CBitSet &right);
    // Объединить два множества

    ~CBitSet(void);
}; 

// Создать множество, допускающее элементы от 0..nlength-1
CBitSet::CBitSet(int nlength) {
    m_length = nlength;
    m_set = new bool[nlength];
    for (int i = 0; i < nlength; i++) m_set[i] = false;
}

// Проверить наличие в множестве элемента index
bool CBitSet::check(int index) const {
    if ((index < 0) || (index >= m_length)) return false;
    return m_set[index];
}

// Узнать размерность множества
int CBitSet::getLength() const {
    return m_length;
}

CBitSet::~CBitSet(void) {
    delete m_set;
}

// Включить в множество элемент index
void CBitSet::set(int index) {
    m_set[index] = true;
}

// Исключить элемент index из множества
void CBitSet::reset(int index) {
    m_set[index] = false;
}

// Исключить все элементы
void CBitSet::reset() {
    for (int i=0; i < m_length; i++)
    m_set[i] = false;
}

// Сравнить два множества
bool CBitSet::operator< ( const CBitSet &right) const {
    if (m_length < right.m_length) return true;
    if (m_length > right.m_length) return false;
    for (int i = 0; i < m_length; i++)
    if (m_set[i] ^ right.m_set[i]) {
        if (m_set[i]) return true;
        else return false;
    };
    return false;
} 

// Объединить два множества
CBitSet::operator += (const CBitSet &right) {
    int a = m_length;
    if (right.m_length < a)
    a = right.m_length;
    for (int i = 0; i < a; i++)
    m_set[i] |= right.m_set[i];
    return 0;
}

// Конструктор присваивания
CBitSet::CBitSet(const CBitSet& right) {
    m_length = right.m_length;
    m_set = new bool[m_length];
    memcpy(m_set, right.m_set, m_length*sizeof(bool));
}

// Конструктор копирования
CBitSet& CBitSet::operator =(const CBitSet& right) {
    if (this != &right) {
        m_length = right.m_length;
        delete m_set;
        m_set = new bool[m_length];
        memcpy(m_set, right.m_set, m_length*sizeof(bool));
    };
    return *this;
}