/* CLexems - класс предназначен для разбора файлов на лексемы. Он используется
для чтения входного потока и его разбора */
enum TLexem {TL_ENDFILE, TL_OPENBRACE, TL_CLOSEBRACE, TL_IDENTY,
 TL_OPENFILE, TL_SEMICOLON, TL_KEYSTATE, TL_KEYFINAL,
 TL_KEYSTART, TL_COMMA, TL_UNKNOWN};
// Стандартные лексемы
// Класс, разбивающий входной поток на лексемы
class CLexems{
    private:
        istream* m_instream; // Входной поток
        char m_curchar; // Текущий символ
        char* m_buffer; // Текущее слово

        TLexem m_curLexem; // Текущая лексема
        void nextChar();
        // Считать следующий символ

        bool ifSkipChar(char ch);
        // Проверить символ на принадлежность к "белым" пробелам

        void scanWord();
        // Считать слово

        void skipToLineEnd();
        // Пропустить все символы до конца строки

        void skipToCommentEnd();
        // Пропустить все символы до символов */

        int m_countread, m_countprev;
        // Общее количество прочитанных символов, и прочитанных к моменту начала разбора предыдущей лексемы
    public:
        CLexems(void);

        CLexems(istream& s_in);
        // Создать класс, и сразу подать ему на вход поток

        void openStream(istream& s_in);
        // Открыть поток

        TLexem getCurrentLexem();
        // Узнать текущую лексему

        TLexem nextLexem();
        // Прочитать следующую лексему и узнать её

        void compare(TLexem now, TLexem exp);
        // Сравнить две лексемы - одна текущая, другая ожидаемая

        TLexem throwBad(CString st);
        // Вызвать исключение, обругавшись на вход

        const char* getString();
        // Получить текущее слово

        ~CLexems(void);
    }; 

const int maxbuffer = 10000;
const int CH_ENDFILE = 0;

// Узнать текущую лексему
TLexem CLexems::getCurrentLexem() {
    return m_curLexem;
}

// Проверить символ на принадлежность к "белым" пробелам
bool CLexems::ifSkipChar(char ch) {
    if (m_curLexem == TL_ENDFILE) return false;

    switch (ch) {
        case ' ': 
            return true; 
            break;
        case '\n': 
            return true; 
            break;
        case 13: 
            return true; 
            break;
        case '\t': 
            return true; 
            break;
        default: 
            return false;
    };
}

// Считать следующий символ
void CLexems::nextChar() {
    if (m_curLexem == TL_ENDFILE) return;
    if (m_instream->eof()) m_curchar = CH_ENDFILE;
    else {
        try {
            m_instream->read(&m_curchar, 1);
            m_countread++;
        }
        catch (...) {
            m_curchar = CH_ENDFILE;
        };
    };
}

// Считать слово
void CLexems::scanWord() {
    int i = 0;
    while (isalpha(m_curchar) || isdigit(m_curchar) || (m_curchar == '_')) {
        m_buffer[i++] = m_curchar;
        nextChar();
    };
    m_buffer[i] = 0;

    m_curLexem = TL_IDENTY;
    if (strcmp(m_buffer, "state") == 0) m_curLexem = TL_KEYSTATE;
    if (strcmp(m_buffer, "final") == 0) m_curLexem = TL_KEYFINAL;
    if (strcmp(m_buffer, "start") == 0) m_curLexem = TL_KEYSTART;
};

// Пропустить все символы до конца строки
void CLexems::skipToLineEnd() { 
    while ((m_curchar != '\n') && (m_curchar != CH_ENDFILE)) nextChar();
    nextChar();
}

// Пропустить все символы до символов */
void CLexems::skipToCommentEnd() {
    int cur = 0;
    while ((m_curchar != CH_ENDFILE) && (cur != 2)) {
        nextChar();
        if (m_curchar == '*') cur = 1;
        if ((cur == 1) && (m_curchar == '/')) cur = 2;
    };
    nextChar();
}

// Прочитать следующую лексему и узнать её
TLexem CLexems::nextLexem() {
    if (m_curLexem == TL_ENDFILE) return m_curLexem;
    m_countprev = m_countread;
    while (ifSkipChar(m_curchar)) nextChar();
    if (isalpha(m_curchar)) scanWord();
    else {
        switch (m_curchar) {
            case '_': 
                scanWord(); 
                break;
            case '{': 
                m_curLexem = TL_OPENBRACE; 
                nextChar(); 
                break;
            case '}': 
                m_curLexem = TL_CLOSEBRACE; 
                nextChar(); 
                break;
            case ';': 
                m_curLexem = TL_SEMICOLON; 
                nextChar(); 
                break;
            case ',': 
                m_curLexem = TL_COMMA; 
                nextChar(); 
                break;
            case '/': 
                nextChar();
                switch(m_curchar) {
                    case '/': 
                        skipToLineEnd(); 
                        m_curLexem = nextLexem(); 
                        break;
                    case '*': 
                        skipToCommentEnd();
                        m_curLexem = nextLexem(); 
                        break;
                    default: 
                        m_curLexem = TL_UNKNOWN; 
                        break;
                };
                break;
            case CH_ENDFILE: 
                m_curLexem = TL_ENDFILE; 
                break;
            default: 
                m_curLexem = TL_UNKNOWN; 
                nextChar(); break;
        }
    }
    return m_curLexem;
}

// Получить текущее слово
const char* CLexems::getString() {
    return m_buffer;
}

CLexems::CLexems(void) {
    m_buffer = new char[maxbuffer];
    m_buffer[0] = 0;
    m_curchar = ' ';
    m_curLexem = TL_OPENFILE;
    m_countread = 0;
    m_countprev = 0;
    m_instream = NULL;
}

// Создать класс, и сразу подать ему на вход поток
CLexems::CLexems(istream& s_in) { 
    m_buffer = new char[maxbuffer];
    m_buffer[0] = 0;
    m_curchar = ' ';
    m_instream = &s_in;
    m_curLexem = TL_OPENFILE;
    m_countread = 0;
    m_countprev = 0;
    nextLexem();
}
// Открыть поток
void CLexems::openStream(istream& s_in) {
    m_instream = &s_in;
    m_curLexem = TL_OPENFILE;
    m_countread = 0;
    m_countprev = 0;
    nextLexem();
}

CLexems::~CLexems(void) {
    delete m_buffer;
}

// Сравнить две лексемы - одна текущая, другая ожидаемая
void CLexems::compare(TLexem now, TLexem exp) {
    if (now != exp) {
        switch (exp) {
            case TL_ENDFILE: 
                throwBad("Expected end of description"); 
                break;
            case TL_OPENBRACE: 
                throwBad("Expected open brace"); 
                break;
            case TL_CLOSEBRACE: 
                throwBad("Expected close brace"); 
                    break;
            case TL_IDENTY: 
                throwBad("Expected identifier"); 
                break;
            case TL_SEMICOLON: 
                throwBad("Expected semicolon"); 
                break;
            case TL_KEYSTATE: 
                throwBad("Expected keyword state"); 
                break;
            case TL_KEYFINAL: 
                throwBad("Expected keywrod final"); 
                break;
            case TL_KEYSTART: 
                throwBad("Expected keyword start"); 
                break;
            case TL_COMMA: 
                throwBad("Expected comma"); 
                break;
        };
    }
}

// Вызвать исключение, обругавшись на вход
TLexem CLexems::throwBad(CString st) {
    CString num;
    num.Format("%i ", m_countprev);
    throw num + st;
}