/* CListWord - класс предназначен для хранения в АВЛ - дереве пары - строка и
число */
typedef map<CString, int> MAPSTRING;
class CListWord {
    private:
        vector<char*> m_names; // Список имен
        MAPSTRING m_idkeys;
        // АВЛ-дерево, отсортированное по первому ключу
    public:
        CListWord(void);
        ~CListWord(void);
        CString getListNames();
        // Получить список всех имен, записанный в строку, с символами конца строки
        void getVectorNames(vector<const char*>& list);
        // Получить список всех имен
        int getCount() const;
        // Узнать количество пар
        int addWord(const char *name);
        // Получить id по имени, если имени нет, то оно добавляется
        int findWord(const char *name);
        // Найти id по имени, если имени нет, то возвращает -1
        const char* findById(int id);
        // Найти имя по индексу
        void clearList();
        // Очистить пары
        bool deleteByMask(const CBitSet* set);
        // Удалить пары, в которых индексу не из множества set
};

CListWord::CListWord(void) {
    m_idkeys.clear();
    m_names.clear();
}
CListWord::~CListWord(void) {
    clearList();
}

// Получить индекс по имени, если имени нет, то оно добавляется
int CListWord::addWord(const char *name) {
    MAPSTRING::iterator it = m_idkeys.find(name);
    if ((it != m_idkeys.end()) && (strcmp(it->first, name) == 0)) {
        return it->second;
    }
    char* sname = strdup(name);
    int id = getCount();
    m_names.push_back(sname);
    m_idkeys.insert(MAPSTRING::value_type(sname, id));
    return id;
}

// Найти индекс по имени, если имени нет, то возвращает -1
int CListWord::findWord(const char *name) {
    MAPSTRING::iterator it = m_idkeys.find(name);
    if ((it != NULL) && (strcmp(it->first, name) == 0)) return it->second;
    return -1;
}

// Найти имя по id
const char* CListWord::findById(int id) {
    if ((id < 0 ) || (id >= getCount())) return NULL;
    return m_names[id];
}

// Очистить пары
void CListWord::clearList() {
    for (int i = 0; i < getCount(); i++) {
        char* name = m_names[i];
        delete name;
    }
    m_names.clear();
    m_idkeys.clear();
}

// Удалить пары, в которых id не из множества set
bool CListWord::deleteByMask(const CBitSet* set) {
if (!deleteVectorByMask<char> (&m_names, set)) return false;
    m_idkeys.clear();
    for (int i = 0; i < getCount(); i++) {
        m_idkeys.insert(MAPSTRING::value_type(m_names[i], i));
    }
    return true;
}

// Узнать количество пар
int CListWord::getCount() const{
    return m_names.size();
}

// Получить список всех имен
void CListWord::getVectorNames(vector<const char*>& list) {
    list.clear();
    for (int i = 0; i < getCount(); i++) list.push_back(m_names[i]);
}

// Получить список всех имен, записанные в строку, разделенные концом строки
CString CListWord::getListNames() {
    CString res;
    int len = 0;
    for (int i = 0; i < getCount(); i++) len += 3 + strlen(m_names[i]);
    res.GetBuffer(len);
    res.Empty();
    for (i = 0; i < getCount(); i++) {
        res += CString(m_names[i]);
        res += CString('\n');
    };
    return res;
}