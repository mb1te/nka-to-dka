/* CState - класс хранит состояние автомата. Он содержит данные о переходах из
этих состояний, а также различные методы, вспомогательные по отношению к методам
класса CAutomaton */
class CState {
    friend class CAutomaton;
    private:
        CAutomaton* m_liststates; // Указатель на "свой" автомат
        bool m_property_final; // Свойство конечное состояние
        bool m_property_start; // Свойство начальное состояние

        vector <POINT> m_idevents; // Список внешних воздействий

        int m_myOwnId; // Индекс состояния в списке всех состояний

        void sortLinks();
        // Отсортировать список внешних воздействий
        
        void processAnyState();
        bool ifSorted();
        // Проверить список внешних воздействий на упорядоченность

        bool transformToDeterminate(C2MapBitSet* numerate, int countStart);
        // Преобразовать состояние в детерминированное

        void goneRound(CBitSet* visited);
        // Посетить всех соседей и отметить их в множестве visited

        bool m_ELSEEVENTlink;
        // Существует ли переход ELSE

        void refreshLinks(vector<int> &newids);
        // Изменить идентификаторы состояний, в которые делаются переходы.
        // Число "-1" означает, что состояние удаляется
    public:
        CState(CAutomaton *parent, int idthat);
        // Создать вершину заданного автомата и заданным индексом

        ~CState(void);
        bool checkStateDeterminate();
        // Проверить состояние на “детерминированность”

        int getThisId();
        // Узнать id состояния

        int getCountLinks() const;
        // Получить количество переходов

        void addCrossing(int event, int nextstate);
        // Добавить переход

        void unionWith(CState* right);
        // Объединить два состояния

        int crossByEvent(int eventid);
        // Номер состояния, куда совершен переход по внешнему воздействию

        void setFinalProperty(bool p_final);
        // Установить свойство состояния быть конечным

        void setStartProperty(bool p_start);
        // Установить свойство состояния быть начальным

        bool checkStartProperty();
        // Проверить свойство состояния быть начальным

        bool checkFinalProperty();
        // Проверить свойство состояния быть конечным

        bool checkELSEEVENTLink();
        // Проверить есть ли переход по внешнему воздействию ELSE

        ostream& operator<< (ostream&);
        // Сохранить состояние в выходной поток
};

int IDELSEEVENT = 1;
int IDANYEVENT = 0;

bool POINTless(POINT p1, POINT p2) {
    return ((p1.x < p2.x) || ((p1.x == p2.x) && (p1.y < p2.y)));
}

// Получить список всех имен, записанные в строку, разделенные концом строки
CState::CState(CAutomaton *par, int idthat) {
    if (par == NULL) throw "Null pointer assignment";
    m_liststates = par;
    m_myOwnId = idthat;
    m_property_final = false;
    m_property_start = false;
    m_idevents.clear();
    m_ELSEEVENTlink = false;
}

CState::~CState(void) {
    m_idevents.clear();
}

// Узнать индекс состояния
int CState::getThisId() {
    return m_myOwnId;
}

// Добавить переход
void CState::addCrossing(int event, int nextstate) {
    POINT c = {event, nextstate};
    if (event == IDELSEEVENT) m_ELSEEVENTlink = true;
    m_idevents.push_back(c);
}

// Проверить есть ли переход по событию ELSEEVENT
bool CState::checkELSEEVENTLink() {
    return m_ELSEEVENTlink;
}

// Проверить состояние на детерминированность
bool CState::checkStateDeterminate() {
    sortLinks();
    for (int i = 0; i < getCountLinks() - 1; i++)
    if (m_idevents[i].x == m_idevents[i + 1].x) return false;

    return true;
} 

// Преобразовать состояние в детерминированное
bool CState::transformToDeterminate(C2MapBitSet* numerate, int countStart) {
    sortLinks();
    int i = 0, j = 0;

    while (i < getCountLinks()) {
        j = i;
        while ((j < getCountLinks() - 1) && (m_idevents[j].x == m_idevents[j + 1].x)) j++;
        if (j != i) {
            CBitSet set = CBitSet(countStart);
            for (int k = i; k <= j; k++) {
                set += *(numerate->findBySecond(m_idevents[k].y));
            };

            int idnew = (numerate->findByFirst(set));
            CState* newstate;
            if (idnew < 0) {
                newstate = m_liststates->createStateRandom();
                idnew = newstate->getThisId();
                numerate->add(set, idnew);
                for (int k = i; k <= j; k++) {
                    newstate->unionWith(m_liststates->getStateByIndex(m_idevents[k].y));
                }
            };

            while (i <= j) {
                m_idevents[i].y = idnew;
                i++;
            };
        }
        else i++;
    };
    sortLinks();
    return true;
}

// Обработать ANY переходы
void CState::processAnyState() {
    vector <POINT> idnew;
    idnew.clear();
    idnew.swap(m_idevents);
    for (int i = 0; i < (int) idnew.size(); i++) {
        if (idnew[i].x == IDANYEVENT) {
            for (int j = 0; j < (int) idnew.size(); j++) {
                if (idnew[j].x != IDANYEVENT) addCrossing(idnew[j].x, idnew[i].y);
            }
            addCrossing(IDELSEEVENT, idnew[i].y);
        } 
        else addCrossing(idnew[i].x, idnew[i].y);
    }
}

// Отсортировать список внешних воздействий
void CState::sortLinks() {
    std::sort(m_idevents.begin(), m_idevents.end(), POINTless);
    vector <POINT> idnew;
    idnew.clear();
    idnew.swap(m_idevents);
    for (int i = 0; i < (int) idnew.size(); i++) {
        POINT c = idnew[i];
        if (i == 0) m_idevents.push_back(idnew[i]);
        else if (POINTless(idnew[i-1], idnew[i])) m_idevents.push_back(idnew[i]);
    };
    idnew.clear();
}

typedef vector<POINT> intvector;
typedef intvector::iterator iterint;

// Сравнить данные, записанные в векторе. За концом вектора - плюс бесконечность
int compare(iterint& lf, iterint& rg, const iterint& lfend, const iterint& rgend) {
    if (lf == lfend) return 1;
    if (rg == rgend) return -1;
    int li = lf->x, ri = rg->x;
    if (li < ri) return -1;
    if (ri > li) return 1;
    return 0;
}

// Добавить все ссылки из второго вектора, которые пропущены в первом
void addaoth(iterint& itf, intvector& oth, intvector& addon) {
    iterint mv = oth.begin();
    POINT c;
    while (mv != oth.end()) {
        if (mv->x != IDELSEEVENT) break ;
        c.x = itf->x;
        c.y = mv->y;
        addon.push_back(c);
        mv++;
    };
}

// Пропустить одинаковые числа в векторах
void skipeq(iterint& itf, intvector& itwhere) {
    if (itf == itwhere.end()) return;
    int sp = itf->x;
    while (itf != itwhere.end()) {
        int nw = itf->x;
        if (nw != sp) return;
        itf++;
    };
}

// Объединить два состояния
void CState::unionWith(CState* right) {
    if (right == NULL) return;

    m_property_final |= right->m_property_final;
    m_ELSEEVENTlink |= right->m_ELSEEVENTlink;
    sortLinks(); 
    right->sortLinks();
    intvector addon;
    addon.clear();
    iterint itf, its;
    itf = m_idevents.begin();
    its = right->m_idevents.begin();

    if ((itf != m_idevents.end()) && (itf->x == IDELSEEVENT)) skipeq(itf, m_idevents);
    if ((its != right->m_idevents.end()) && (its->x == IDELSEEVENT)) skipeq(its, right->m_idevents);
    while ((itf != m_idevents.end()) || (its != right->m_idevents.end())) {
        switch (compare(itf, its, m_idevents.end(), right->m_idevents.end())) {
            case -1: 
                addaoth(itf, right->m_idevents, addon); 
                itf++; 
                break;
            case 0: 
                skipeq(itf, m_idevents);
                skipeq(its, right->m_idevents); 
                break;
            case 1: 
                addaoth(its, m_idevents, addon); 
                its++; 
                break;
        };
    };
    m_idevents.insert(m_idevents.end(),
    right->m_idevents.begin(),
    right->m_idevents.end());
    m_idevents.insert(m_idevents.end(), addon.begin(), addon.end());
}

// Установить свойство состояния быть конечным
void CState::setFinalProperty(bool p_final) {
    m_property_final = p_final;
}

// Установить свойство состояния быть начальным
void CState::setStartProperty(bool p_start) {
    m_property_start = p_start;
}

// Сохранить состояние в выходной поток
ostream& operator<< (ostream& out, CState* state) {
    return (state->operator <<(out));
}

// Сохранить состояние в выходной поток
ostream& operator<< (ostream& out, CState& state) {
    return (state.operator <<(out));
}

// Сохранить состояние в выходной поток
ostream& CState::operator<< (ostream& out) {
    out << "\t";
    sortLinks();
    if (m_property_start) out << "start ";
    if (m_property_final) out << "final "; 
    out << "state " << m_liststates->getStateNameByIndex(m_myOwnId) << " {\n";
    for (int j = 0; j < 2; j++) {
        for (int i = 0; i < getCountLinks(); i++) {
            if ((m_idevents[i].x != IDELSEEVENT) ^ j) {
                out << "\t\t";
                out << m_liststates->getEventNameByIndex(m_idevents[i].x);
                out << '\t';
                out << m_liststates->getStateNameByIndex(m_idevents[i].y);
                out << ";\n";
            };
        }
    }
    out << "\t} // End of state " <<
    m_liststates->getStateNameByIndex(m_myOwnId) << "\n";
    return out;
}

// Проверить свойство состояния быть начальным
bool CState::checkStartProperty() {
    return m_property_start;
}

// Проверить свойство состояния быть конечным
bool CState::checkFinalProperty() {
    return m_property_final;
}

// Посетить всех соседей и отметить их в множестве visited
void CState::goneRound(CBitSet* visited) {
    int id = getThisId();
    if (visited->check(id)) return;
    visited->set(id);
    for (int i = 0; i < getCountLinks(); i++) {
        CState *state = m_liststates->getStateByIndex(m_idevents[i].y);
        state->goneRound(visited);
    }
}

// Получить количество переходов
int CState::getCountLinks() const {
    return m_idevents.size();
}

// Изменить идентификаторы состояний, в которые делаются переходы. Число "-1" означает, что состояние удаляется
void CState::refreshLinks(vector<int> &newids) {
    for (int i = 0; i < getCountLinks(); i++)
        m_idevents[i].y = newids[m_idevents[i].y];
    }
}
    
// Возможно ли присваивание
bool assignment(int& lastval, int newval) {
    if (newval == -1) newval = lastval;
    if (lastval == -1) lastval = newval;
    return (lastval == newval);
} 

// Номер состояния, куда совершен переход по внешнему воздействию
int CState::crossByEvent(int eventid) {
    int onAny = -1, onElse = -1, onThis = -1;
    for (int i = 0; i < getCountLinks(); i++) {
        if (m_idevents[i].x == eventid)
        if (!assignment(onThis, m_idevents[i].y)) return -1;
        if (m_idevents[i].x == 1)
        if (!assignment(onElse, m_idevents[i].y)) return -1;
        if (m_idevents[i].x == 0)
        if (!assignment(onAny, m_idevents[i].y)) return -1;
    };
    if (!assignment(onThis, onAny)) return -1;
    if (onThis != -1) return onThis;
    else return onElse;
}