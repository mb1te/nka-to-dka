// Это вспомогательный файл, содержащий в себе одну функцию
// Шаблон, позволяющий удалять из вектора по маске
template <class TVecElem>
bool deleteVectorByMask(vector<TVecElem*> *vec, const CBitSet* mask) {
    if (vec->size() != mask->getLength()) return false;
    vector<TVecElem*> old;
    old.clear();
    old.swap(*vec);
    for (int i = 0; i < mask->getLength(); i++) {
        if (mask->check(i)) vec->push_back(old[i]);
        else delete (old[i]);
    }
    return true; 
}